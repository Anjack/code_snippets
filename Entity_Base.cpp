// Fill out your copyright notice in the Description page of Project Settings.

#include "Entity_Base.h"


// Sets default values
AEntity_Base::AEntity_Base()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

// Called when the game starts or when spawned
void AEntity_Base::BeginPlay()
{
	Super::BeginPlay();
	
	is_dead = false;
}

// Called every frame
void AEntity_Base::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEntity_Base::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

/*
 *	Entities should implement their own custom attacks by overriding this function.
 *	However, all entities should have a base melee attack which will eventually be 
 *	handled here.
 */
bool AEntity_Base::TryAttackEntity(AActor * attack_target)
{
	bool result = false;
	// By default, the attack fails
	result = TryMeleeAttack(attack_target);

	return result;
}
// This is where we will eventually handle the basic melee attack that anyone can do.
bool AEntity_Base::TryMeleeAttack(AActor * attack_target)
{
	return false;
}

// If an entity needs special procedures for death, override this function.
void AEntity_Base::DieGracefully()
{
	/*if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, GetName() + ": died gracefully.");
	}*/

	is_dead = true;

	this->OnDeath();
}

/*
 *	All entities can be hit. Fire this event when that happens.
 *	If we need to do something extra, e.g. a special attack, override this (but still call it 
 *	to handle basic damage, if the special attack does damage.)
 */
void AEntity_Base::TakeHit_Implementation(AActor* damage_src, int32 damage_amt)
{
	// Everyone should have a core_body value to track their health. When we get hit, subtract
	// the damage we take.
	core_body -= damage_amt;
	// Raise a second event after we take damage, for special cases.
	OnHit();
	// If we have no more core_body, we die.
	if (core_body <= 0) {
		OnDeath();
	}
}

// Raise an event that can be intercepted by Blueprints. Mostly used for animations.
void AEntity_Base::OnDeath_Implementation() 
{
}
// Raise an event that can be intercepted by Blueprints. Mostly used for animations.
void AEntity_Base::OnHit_Implementation() 
{
}
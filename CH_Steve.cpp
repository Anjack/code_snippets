
#include "CH_Steve.h"
#include "Components/SphereComponent.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"


// Sets default values
ACH_Steve::ACH_Steve()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Constructor
ACH_Steve::ACH_Steve(const FObjectInitializer & ObjectInitializer)
{
	// Spring arm for camera.
	CameraBoomComp = ObjectInitializer.CreateDefaultSubobject<USpringArmComponent>(this, TEXT("CameraBoom"));
	
	// Default values: socket is start, target is position of camera.
	CameraBoomComp->SocketOffset = FVector(0, 35, 0);
	CameraBoomComp->TargetOffset = FVector(0, 0, 55);

	// To "attach" the camera to the character.
	CameraBoomComp->bUsePawnControlRotation = true;
	CameraBoomComp->AttachTo(GetRootComponent());

	// Camera attached to the spring arm.
	CameraComp = ObjectInitializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("Camera"));
	CameraComp->AttachTo(CameraBoomComp);

	// Initialize the pickup_detection_sphere.
	detection_sphere = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("CollisionDetectionSphere"));
	detection_sphere->InitSphereRadius(350.0f);
	detection_sphere->SetVisibility(true);
	detection_sphere->SetHiddenInGame(true);
	detection_sphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	detection_sphere->SetupAttachment(GetRootComponent());

	// Reference to my mesh component.
	MySkellie = GetMesh();

}

// Called when the game starts or when spawned
void ACH_Steve::BeginPlay()
{
	// Call parent startup routines.
	Super::BeginPlay();
	
	/*if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, TEXT("Initializing Steve..."));
	}*/

	// Populate the array.
	SetupClay_Map();
	InitializeStats();
	CalculateTotalBodyAndVitae();

	// Set invulnerability timer
	default_invul_time = 2;
	invul_timer = default_invul_time;
}

// Called every frame
void ACH_Steve::Tick(float DeltaTime)
{
	// Execute parent tick.
	Super::Tick(DeltaTime);
	// For tracking temporary invulnerability when we spawn.
	invul_timer -= DeltaTime;
}

// Initial body/vitae values.
// TODO: Pull from config file?
void ACH_Steve::InitializeStats() {
	this->core_body = 10;
	my_vitae = 0;
}


// This will give the total body value of the base actor and all attached body parts.
void ACH_Steve::CalculateTotalBodyAndVitae() {
	// Get the next highest integer for our core body value.
	int32 temp_body = FMath::CeilToInt(this->core_body);
	// Then cycle through each attached body part and add its value.
	for (auto& item : clay_map) {
		if (item.Value != nullptr) {
			temp_body += FMath::CeilToInt(item.Value->body);
		}
	}
	// The result is our current total body.
	total_body = temp_body;

	// Vitae has not yet been implemented.
}

// Populate the clay map
// TODO: This should pull the list of existing sockets in the mesh and populate from there.
void ACH_Steve::SetupClay_Map() {
	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, TEXT("Populating clay_array..."));
	}
	clay_map.Add("lowerarm_l_socket", nullptr);
	clay_map.Add("lowerarm_r_socket", nullptr);
	clay_map.Add("upperarm_l_socket", nullptr);
	clay_map.Add("upperarm_r_socket", nullptr);
	clay_map.Add("thigh_l_socket", nullptr);
	clay_map.Add("thigh_r_socket", nullptr);
	clay_map.Add("calf_l_socket", nullptr);
	clay_map.Add("calf_r_socket", nullptr);
	clay_map.Add("torso_socket", nullptr);
	clay_map.Add("head_socket", nullptr);
}

// Called to bind functionality to input
void ACH_Steve::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Keyboard Controls
	InputComponent->BindAxis("MoveForward", this, &ACH_Steve::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ACH_Steve::MoveRight);
	InputComponent->BindAction("Jump", IE_Pressed, this, &ACH_Steve::OnStartJump);
	InputComponent->BindAction("Jump", IE_Released, this, &ACH_Steve::OnStopJump);
	InputComponent->BindAction("PickUp", IE_Pressed, this, &ACH_Steve::AttemptToGrabPickup);


	// Mouse Controls
	InputComponent->BindAxis("Turn", this, &ACH_Steve::AddControllerYawInput);
	InputComponent->BindAxis("LookUp", this, &ACH_Steve::AddControllerPitchInput);

	// Gamepad Controls
	InputComponent->BindAxis("TurnRate", this, &ACH_Steve::AddControllerYawInput);
	InputComponent->BindAxis("LookupRate", this, &ACH_Steve::AddControllerPitchInput);
}

void ACH_Steve::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// Find forward
		FRotator Rotation = Controller->GetControlRotation();

		// Don't want to PITCH over! :D
		if (GetCharacterMovement()->IsMovingOnGround() || GetCharacterMovement()->IsFalling())
		{
			Rotation.Pitch = 0.0f;
		}

		// Apply movement in the proper direction.
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ACH_Steve::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// Find direction right.
		const FRotator Rotation = Controller->GetControlRotation();
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::Y);
		
		// Apply movement.
		AddMovementInput(Direction, Value);
	}
}

void ACH_Steve::OnStartJump()
{
	// Used in the animation blueprint.
	bPressedJump = true;
}

void ACH_Steve::OnStopJump()
{
	// Used in the animation blueprint.
	bPressedJump = false;
}
/*
 *	Simple function to locate nearest actor, used when interacting with the game world.
 */
AActor* ACH_Steve::FindNearestActorInRange(TSubclassOf<AActor> class_filter = nullptr, AActor* ignored_actor = nullptr) {

	// Get all actors within range
	detection_sphere->GetOverlappingActors(detected_actors, class_filter);

	AActor* nearest_actor = nullptr;
	// If we found any
	if (detected_actors.Num() != 0 && detected_actors[0] != nullptr) {

		for (AActor* actor_to_check : detected_actors) {
			// Check if it's on the list of actors we don't care about
			if (actor_to_check != ignored_actor) {
				if (nearest_actor == nullptr) {
					// If we haven't found any other actors, yet, let's consider this the closest.
					nearest_actor = actor_to_check;
				}
				else if (FVector::Dist(actor_to_check->GetActorLocation(), GetActorLocation()) < FVector::Dist(nearest_actor->GetActorLocation(), GetActorLocation())) {
					// If this actor is closer than the current closest, it wins the title.
					nearest_actor = actor_to_check;
				}
			}
		}
	}
	// Is this cleanup necessary?
	detected_actors.Empty();

	
	// Finally, return the result.
	return nearest_actor;
}

/*
 *	When player presses the "pick up" button, look for appropriate actors within range.
 */
void ACH_Steve::AttemptToGrabPickup() {
	AActor* pickup_target = FindNearestActorInRange(APickUp_Base::StaticClass());
	// If a pick-up was found, process it.
	if (pickup_target != nullptr) {
		ACH_Steve::ProcessPickUp(pickup_target);
	}
}

void ACH_Steve::ProcessPickUp(AActor* picked_up_item)
{
	if (picked_up_item->IsA(AClay_Pickup::StaticClass())) {
		// ***********Temporarily moved this to blueprint for faster debuging.***********

		//for (auto& item : clay_map) {
		//	if (item.Value == nullptr) {
		//		
		//		// If we have an open socket, give Steve a relevant body part.

		//		picked_up_item->Destroy();

		//		/*
		//		UObject* temp_object = FindObject<ABodyPart>(this, TEXT("Blueprint'/Game/OVA_Custom/BodyParts/BPClay_BodyPart.BPClay_BodyPart'"));
		//		UClass * temp_class = temp_object->StaticClass();

		//		ABodyPart* new_body_part = (ABodyPart*)GetWorld()->SpawnActor(temp_class);

		//		FActorSpawnParameters SpawnParameters;
		//		ABodyPart* new_body_part = GetWorld()->SpawnActor<ABodyPart>(BodyPartBP, GetTransform(), SpawnParameters);

		//		new_body_part->AttachRootComponentTo(MySkellie, item.Key, EAttachLocation::SnapToTarget, true);
		//		
		//		item.Value = new_body_part;
		//		*/
		//		CalculateTotalBodyAndVitae();
		//		break;
		//	}
		//}

	}
	else if (picked_up_item->IsA(AItem_Base::StaticClass())) {
		/*if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Orange, TEXT("Found a thing!"));
		}*/

		// Cast the item so we can access the member properties/methods.
		AItem_Base* picked_item = Cast<AItem_Base>(picked_up_item);
		
		FInventoryItem inv_item;
		
		if (picked_item->Tags.Num() > 0) {
			// The name of the item should be the first tag.
			inv_item.item_name = picked_item->Tags[0].ToString();
		}
		else {
			// If not, then call it by its proper name.
			inv_item.item_name = picked_item->GetFName().ToString();
		}
		// Special case for weapon pick-up: automatically "equip" the weapon.
		if (picked_item->ActorHasTag(TEXT("spark_gun"))) {
			TArray<UActorComponent*> gun_spots = GetComponentsByTag(UStaticMeshComponent::StaticClass(), TEXT("spark_gun"));
			// If this is the first time we pick it up, make it visible on the character.
			if (gun_spots.Num() > 0) {
				Cast<USceneComponent>(gun_spots[0])->SetHiddenInGame(false);
			}
		}
		// Add the item to our inventory, then destroy the actor.
		inventory.Add(inv_item);
		picked_item->Destroy();
	}
}

// ***********HANDLED IN BP NOW***********
// Override default behavior to handle all incomine damage ("hits")
//void ACH_Steve::TakeHit_Implementation(AActor * damage_src, int32 damage_amt, TArray<UPrimitiveComponent*>& overlapped_components)
//{
//	/*if (invul_timer <= 0) {
//		invul_timer = default_invul_time;
//		TArray<AActor*> temp_overlapped_actors;
//
//		damage_src->GetOverlappingActors(temp_overlapped_actors);
//
//		TArray<ABodyPart*> overlapped_body_parts;
//
//		for (auto& item : temp_overlapped_actors) {
//			if (item!= nullptr && item->IsA(ABodyPart::StaticClass()) && !overlapped_body_parts.Contains(item)) {
//				overlapped_body_parts.Add((ABodyPart*)item);
//			}
//			else if (item->IsA(ACH_Steve::StaticClass())) {
//				for (auto& bp_item : clay_map) {
//					if (bp_item.Value != nullptr) {
//						overlapped_body_parts.Add(bp_item.Value);
//					}
//				}
//			}
//		}
//
//		if (overlapped_body_parts.Num() > 0) {
//			
//			ACH_Steve::DistributeDamage(damage_amt, overlapped_body_parts);
//		}
//		else {
//			Super::TakeHit(damage_src, damage_amt);
//		}
//	}*/
//
//	Super::TakeHit(damage_src, damage_amt, overlapped_components);
//}

/*
* Divide dmg amount by number of parts and apply that average to all parts.
* Negative results will be collected as remaining damage and the body part will be 
* removed from the list. The remaining damage and body part list will be passed back
* to this function again.
* Finally returns when either damage_amt or bp_list are empty.
*/
void ACH_Steve::DistributeDamage(int32 damage_amt, TArray<ABodyPart*> bp_list) {
	float damage_avg = damage_amt / bp_list.Num();
	int32 damage_remain = 0;
	int32 body_result;

	for (ABodyPart* item : bp_list) {
		// Subtract the average damage amount from the current body part.
		body_result = item->body - damage_avg;
		
		if (body_result > 0) {
			item->body = body_result;
		}
		else if (body_result < 0) {
			// If the result is negative, we did more damage then that body part could soak,
			// so we take the remainder (the negative result), multiply by -1 to make it positive,
			// then add back to the total damage remaining.
			damage_remain += -1 * body_result;
			item->body = 0;
		}
		else {
			// The only other scenaior is if the body_result is equal to 0, in which case
			// we set the body value of this body part to 0 as well.
			item->body = 0;
		}
		// If the body value of the current part is 0, that part should be destroyed.
		if (item->body == 0) {

			/*if (GEngine) {
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, item->GetName());
			}*/
			
			FName parent_socket = item->GetAttachParentSocketName();
			if (parent_socket != "" && clay_map.Contains(parent_socket)) {
				// Detatch the body part component or actor.
				clay_map[parent_socket] = nullptr;
			}
			// Stop tracking the body part.
			bp_list.Remove(item);
			// Findally, destroy the actor/component for that body part.
			item->DieGracefully();
		}
	}

	// These should never be less than 0
	if (damage_remain <= 0 || bp_list.Num() <= 0) {
		return;
	}
	else {
		// If there's leftover damage, call this function again, recursively.
		ACH_Steve::DistributeDamage(damage_remain, bp_list);
	}

}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Entity_Base.generated.h"


UCLASS()
class PROJECT_OVA_API AEntity_Base : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEntity_Base();
	// Called when we die.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "DeathSystem")
		void OnDeath();
	// Called when get hit, after we take damage.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Combat System")
		void OnHit();
	// Called when another actor attacks this one, to handle incoming damage.
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Combat System")
		void TakeHit(AActor* damage_src, int32 damage_amt);


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Between when we die and when our actor is destroyed, we still need to track this state.
	UPROPERTY(BlueprintReadOnly)
		bool is_dead;
	// Called when we want to attack another entity.
	UFUNCTION(BlueprintCallable)
		bool TryAttackEntity(AActor* attack_target);
	// Blueprints should call TryAttackEntity, which will call this if/when necessary.
	virtual bool TryMeleeAttack(AActor* attack_target);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Called when body is zero or less
	UFUNCTION(BlueprintCallable)
		virtual void DieGracefully();
	
	// How much body mass (health + armor analog)
	UPROPERTY(BlueprintReadWrite, Category = "Stats")
		float core_body;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/Engine.h"
#include "Entity_Base.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/SphereComponent.h"
#include "PickUp_Base.h"
#include "Clay_Pickup.h"
#include "BodyPart.h"
#include "Item_Base.h"
#include"Project_OVA.h"
#include "CH_Steve.generated.h"

UCLASS()
class PROJECT_OVA_API ACH_Steve : public AEntity_Base
{
	GENERATED_BODY()

private:
	

	UPROPERTY(VisibleAnywhere, Category = "Mesh")
		UStaticMeshComponent* tempMesh;

public:
	// Sets default values for this character's properties
	ACH_Steve();

	// Constructor
	ACH_Steve(const FObjectInitializer& ObjectInitializer);

	// Spring arm for the camera.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Camera")
		USpringArmComponent* CameraBoomComp;
	// The player viewpoint camera.
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Camera")
		UCameraComponent* CameraComp;

	// To store the body parts we pick up.
	UPROPERTY(BlueprintReadWrite)
		TMap<FName, ABodyPart*> clay_map;

	// To store other items we pick up.
	UPROPERTY(BlueprintReadWrite)
		TArray<FInventoryItem> inventory;
	// For crafting.
	UPROPERTY(BlueprintReadWrite)
		TArray<FRecipeItem> recipe_store;
	// Body part object.
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABodyPart> BodyPartBP;
	// To store actors detected within the "detection sphere".
	UPROPERTY(VisibleAnywhere)
		TArray<AActor*> detected_actors;
	// The aforementioned "detection sphere" itself.
	UPROPERTY(BlueprintReadWrite, Category = "Collision")
		USphereComponent* detection_sphere;
	// This is now handled in the Blueprint
	/*UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Combat System")
		void TakeHit(AActor* damage_src, int32 damage_amt, TArray<UPrimitiveComponent*>& overlapped_components);*/

protected:
	// Called when the game starts or when spawned.
	virtual void BeginPlay() override;

	USkeletalMeshComponent* MySkellie;

	// This will apply damage from the TakeHit function.
	void DistributeDamage(int32 damage_amt, TArray<ABodyPart*> bp_list);

	// How long (seconds) are we invulnerable after being hit.
	float invul_timer;

	// Default value for invulnerability timer.
	int default_invul_time;

	// If we're climbing handholds, this is our current handhold.
	AActor* current_handhold;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Set body and vitate and etc. when we spawn.
	void InitializeStats();

	UFUNCTION(BlueprintCallable)
		void CalculateTotalBodyAndVitae();

	// Called once to populate the map with socket names.
	void SetupClay_Map();

	// Called to bind functionality to input.
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Movement functions
	UFUNCTION()
		void MoveForward(float Value);
	UFUNCTION()
		void MoveRight(float Value);
	UFUNCTION()
		void OnStartJump();
	UFUNCTION()
		void OnStopJump();

	// Pickup actions
	// Can we even pick it up?
	UFUNCTION(BlueprintCallable)
		void AttemptToGrabPickup();
	// Checks what kind of item we've picked up and responds accordingly.
	UFUNCTION(BlueprintCallable)
		void ProcessPickUp(class AActor* picked_up_item);
	// Find closest actor within detection_sphere
	UFUNCTION(BlueprintCallable)
		AActor* FindNearestActorInRange(TSubclassOf<AActor> class_filter, AActor* ignored_actor);
	// Health, essentially.
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Stats")
		int32 total_body;
	// Kinda like magic? Not yet implemented.
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Stats")
		int32 my_vitae;
};


## Code Snippets

These are some samples of code I've written. The included comments are mostly intended to clarify functionality. For information about design choices, please contact me directly.

|File Name|Description|
|---|---|
|CH_Steve.cpp|This is the main player character class for a game I'm working on.|
|CH_Steve.h|The header file for CH_Steve.cpp.|
|Entity_Base.cpp|The CH_Steve.cpp class inherits from this class, as do the enemies in the game.	|
|Entity_base.h|Header file for Entity_Base.cpp.|